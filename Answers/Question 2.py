# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 11:09:26 2020

@author: user
"""


import sqlite3
import pandas as pd

conn=sqlite3.connect('''database.sqlite''')
cursor=conn.cursor()

#Question 2 Part 1
cursor.execute('''SELECT COUNT(*) FROM Teams''')
Teams_df=pd.DataFrame(cursor.fetchall())
Teams_df.columns=[x[0] for x in cursor.description]
print(Teams_df)

#Question 2 Part 2
cursor.execute('''SELECT DISTINCT Season
               FROM Teams''')
Teams_df=pd.DataFrame(cursor.fetchall())
Teams_df.columns=[x[0] for x in cursor.description]
print(Teams_df)

#Question 2 Part 3
cursor.execute('''SELECT MAX(StadiumCapacity) AS Largest_Stadium_Capacity
               FROM Teams;''')
Teams_df=pd.DataFrame(cursor.fetchall())
Teams_df.columns=[x[0] for x in cursor.description]
print(Teams_df)

cursor.execute('''SELECT MIN(StadiumCapacity) AS Smallest_Stadium_Capacity
               FROM Teams;''')
Teams_df=pd.DataFrame(cursor.fetchall())
Teams_df.columns=[x[0] for x in cursor.description]
print(Teams_df)

#Question 2 Part 4
cursor.execute('''SELECT SUM(KaderHome) AS Total_Soccer_Player
               FROM Teams
               WHERE Season=2014;''')
Teams_df=pd.DataFrame(cursor.fetchall())
Teams_df.columns=[x[0] for x in cursor.description]
print(Teams_df)

#Question 2 Part 5
cursor.execute('''SELECT AVG(FTHG) AS Average_HomeGoals
               FROM Matches
               WHERE HomeTeam='Man United';''')
Matches_df=pd.DataFrame(cursor.fetchall())
Matches_df.columns=[x[0] for x in cursor.description]
print(Matches_df)