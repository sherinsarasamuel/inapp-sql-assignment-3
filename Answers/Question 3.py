# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 11:46:22 2020

@author: user
"""


import sqlite3
import pandas as pd

conn=sqlite3.connect('''database.sqlite''')
cursor=conn.cursor()

#Question 3 Part 1
cursor.execute('''SELECT HomeTeam,
                         FTHG,
                         FTAG
                   FROM Matches
                   WHERE HomeTeam='Aachen' AND Season=2010
                   ORDER BY FTHG DESC;''')
df=pd.DataFrame(cursor.fetchall())
df.columns=[x[0] for x in cursor.description]
print(df)

#Question 3 Part 2
cursor.execute('''SELECT HomeTeam, COUNT(FTR) AS Total_Home_Wins
               FROM Matches
               WHERE FTR='H' AND Season=2016
               GROUP BY HomeTeam
               ORDER BY COUNT(FTR) DESC;''')
Matches_df=pd.DataFrame(cursor.fetchall())
Matches_df.columns=[x[0] for x in cursor.description]
print(Matches_df)               

#Question 3 Part 3
cursor.execute('''SELECT *
               FROM Unique_Teams
               LIMIT 10;''')
Unique_Teams_df=pd.DataFrame(cursor.fetchall())
Unique_Teams_df.columns=[x[0] for x in cursor.description]
print(Unique_Teams_df)

#Question 3 Part 4
cursor.execute('''SELECT Teams_in_Matches.Match_ID,
                         Teams_in_Matches.Unique_Team_ID,
                         Unique_Teams.TeamName
                 FROM Teams_in_Matches
                 INNER JOIN Unique_Teams
                     ON Teams_in_Matches.Unique_Team_ID=Unique_Teams.Unique_Team_ID
                     LIMIT 5;''')
df=pd.DataFrame(cursor.fetchall())
df.columns=[x[0] for x in cursor.description]
print(df)

#Question 3 Part 5
cursor.execute('''SELECT *
               FROM Unique_Teams
               JOIN Teams
               ON Unique_Teams.TeamName= Teams.TeamName
               LIMIT 10;''')
df=pd.DataFrame(cursor.fetchall())
df.columns=[x[0] for x in cursor.description]
print(df)

#Question 3 Part 6
cursor.execute('''SELECT Unique_Teams.Unique_Team_ID,
                         Unique_Teams.TeamName,
                         Teams.AvgAgeHome,
                         Teams.Season,
                         Teams.ForeignPlayersHome
                 FROM Unique_Teams
                 JOIN Teams
                   ON Unique_Teams.TeamName= Teams.TeamName
               LIMIT 5;''')
df=pd.DataFrame(cursor.fetchall())
df.columns=[x[0] for x in cursor.description]
print(df)

#Question 3 Part 7
cursor.execute('''SELECT MAX(Match_ID),
                         Teams_in_Matches.Unique_Team_ID,
                         TeamName
                 FROM Teams_in_Matches
                 JOIN Unique_Teams
                     ON Teams_in_Matches.Unique_Team_ID=Unique_Teams.Unique_Team_ID
                 WHERE (TeamName LIKE '%y') OR (TeamName LIKE '%r')
                 GROUP BY Teams_in_Matches.Unique_Team_ID, TeamName;''')
df=pd.DataFrame(cursor.fetchall())
df.columns=[x[0] for x in cursor.description]
print(df)



