# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 14:35:45 2020

@author: user
"""

import sqlite3
import pandas as pd

conn=sqlite3.connect('''database.sqlite''')
cursor=conn.cursor()

#Question 1 Part 2
cursor.execute('''SELECT *
             FROM Matches
             WHERE FTHG=3
             AND Season=2015;''')
Matches_df=pd.DataFrame(cursor.fetchall())  #Converts SQL Queries into dataframe format
Matches_df.columns=[x[0] for x in cursor.description] #Labels the columns in dataframe format
print(Matches_df)

#Question 1 Part 2
cursor.execute('''SELECT *
               FROM Matches
               WHERE HomeTeam='Arsenal'
               AND FTR='A';''')
Matches_df=pd.DataFrame(cursor.fetchall())
Matches_df.columns=[x[0] for x in cursor.description]
print(Matches_df)

#Question 1 Part 4
cursor.execute('''SELECT *
               FROM Matches
               WHERE Season BETWEEN 2012
               AND 2015;''')
Matches_df=pd.DataFrame(cursor.fetchall())
Matches_df.columns=[x[0] for x in cursor.description]
print(Matches_df)

#Question 1 Part 5
cursor.execute('''SELECT *
               FROM Matches
               WHERE HomeTeam LIKE 'A%'
               AND AwayTeam LIKE 'M%';''')
Matches_df=pd.DataFrame(cursor.fetchall())
Matches_df.columns=[x[0] for x in cursor.description]
print(Matches_df)               